AJS.$(document).ready(function($) {
	window.setTimeout("AttachRemoteFiles.attachRemoteFileField()", 250);
});

var AttachRemoteFiles = {
		attachRemoteFileField: function () {
			var upfile_parent = AJS.$("#attach-file-dialog,#attachment_div").find(".upfile").parent();
			
			if (upfile_parent.length < 1 || AJS.$("#attach-remote-file-field").length > 0) {
				window.setTimeout("AttachRemoteFiles.attachRemoteFileField()", 250);
				return false;
			}
			
			upfile_parent.after(this.createAttachRemoteFileFieldElement);
			AJS.$("button#attach-remote-file").click(this.attachRemoteFile);
			
			window.setTimeout("AttachRemoteFiles.attachRemoteFileField()", 250);
		},
		
		createAttachRemoteFileFieldElement: function () {
			var element = AJS.$('<div/>', {
				'class': 'field-group',
				'id': 'attach-remote-file-field'
			});
			element.html(AJS.$('<label/>', {
				text: 'by URL',
				'for': 'remote-file-url'
			}));
			element.append(AJS.$('<input/>', {
				'class': 'text',
				'type': 'text',
				'id': 'remote-file-url',
				'name': 'remote-file-url',
				'title': 'attach by URL'
			}));
			
			element.append(AJS.$('<button/>', {
				'class': 'aui-button',
				text: 'attach',
				'id': 'attach-remote-file' 
			}));
			
			return element;
		},
		
		attachRemoteFile: function () {
			AttachRemoteFiles.toggleAttachButton($(this));
			var _form = $(this).parents('form');
			var _url = _form.find("#remote-file-url").val();
			
			AttachRemoteFiles.attachRemoteFileError('', _form);
			
			if (_url.length < 1) {
				AttachRemoteFiles.attachRemoteFileError("No URL provided", _form);
				AttachRemoteFiles.toggleAttachButton($(this));
				return false;
			}
			
			if (!/^(http|ftp|https)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/.test(_url)) {
				AttachRemoteFiles.attachRemoteFileError("Provided URL is not valid", _form);
				AttachRemoteFiles.toggleAttachButton($(this));
				return false;
			}

	        var _params = {
				url: _url,
				atl_token: AttachRemoteFiles.getAtlToken(_form)
			};
	        
	        var _issueId = parseInt(_form.find(":input[name=id]").val());
	        var _projectId = parseInt(_form.find(":input[name=pid]").val());
	        
	        if (!isNaN(_issueId)) {
	        	_params.issueId = _issueId;
	        }
	        else if (!isNaN(_projectId)) {
	        	_params.projectId = _projectId;
	        }
	        else {
	        	AttachRemoteFiles.attachRemoteFileError("Unable to find either an issueId or projectId to submit the attachment to.", _form);
	        }
	        
			AJS.$.ajax({
				type: 'POST',
				url: AJS.params.baseURL+"/rest/attachremotefile/1.0/?" + AJS.$.param(_params),
				success: function (data) {
					var _inlineAttach = new AJS.InlineAttach.Form(new AJS.InlineAttach.FileInput(AJS.$(".upfile"), true));
					_inlineAttach.addTemporaryFileCheckbox(data.id, data.name);
					
					var $element = _form.find("input[value=" + data.id + "]").parent();
					
					if (/image\//i.test(data.contentType)) {
						var $thumbNail = AttachRemoteFiles.addRemoteThumbnailImage(data.name, _url, data.contentType);
						
						if ($thumbNail) {
							$element.append(AJS.$("<br/>")).append($thumbNail);
						}
						
						var addImageButton = document.createElement('button');
						addImageButton.className = "aui-button attach-remote-file-add-image";
						addImageButton.appendChild(document.createTextNode("Add Image tag"));
						var addThumbnailButton = document.createElement('button');
						addThumbnailButton.className = "aui-button attach-remote-file-add-thumbnail";
						addThumbnailButton.appendChild(document.createTextNode("Add Thumbnail tag"));
						var addButtons = document.createElement('p');
						addButtons.className = "aui-buttons attach-remote-file-add-buttons";
						addButtons.appendChild(addImageButton);
						addButtons.appendChild(addThumbnailButton);
						$element.append(addButtons).data('fileName', data.name);
						AttachRemoteFiles.initAddButtonsBinding();
					}
					
					_form.find("#remote-file-url").val('');
				},
				error: function (jqXHR, textStatus, errorThrown) {
					var data = AJS.$.parseJSON(jqXHR.responseText);
					AttachRemoteFiles.attachRemoteFileError(data.errorMessage, _form);
				},
				async: false
			});
			
			AttachRemoteFiles.toggleAttachButton($(this));
			
			return false;
		},
		
		addRemoteThumbnailImage: function (name, url, contentType) {
			var $thumbNail = null;

			if (url && window.URL && window.URL.createObjectURL) {
				if (/image\//i.test(contentType)) {
					var title = name + " - " + contentType;
					var img = document.createElement('img');
					img.className = "attach-remote-file-thumbnail";
					img.title = title;
					img.alt = title;
					img.onload = function(e) {
						var aspectRatio = AttachRemoteFiles.getAspectRatio(100,100,img.width,img.height);
						img.width = Math.round(img.width / aspectRatio);
						img.height = Math.round(img.height / aspectRatio);
						$thumbNail.show();
						window.URL.revokeObjectURL(img.src); // Clean up after yourself.
					};
					img.src = url;
					$thumbNail = jQuery(img).hide();
				}
			}
			return $thumbNail;
		},
		
		getAspectRatio: function(maxWidth,maxHeight, origWidth, origHeight) {
			if (origWidth > maxWidth) {
				return Math.round(origWidth / maxWidth);
			} else if( origHeight > maxHeight) {
				return Math.round(origHeight / maxHeight);
			} else {
				return 1;
			}
		},
		
		toggleAttachButton: function (obj) {
			if (!obj.attr('aria-disabled')) {
				obj.attr('aria-disabled', 'true');
				obj.prepend('<span class="aui-icon aui-icon-wait aui-icon-small"> </span>');
			}
			else {
				obj.attr('aria-disabled', 'false');
				obj.find('span.aui-icon-wait').remove();
			}
		},
		
		getAtlToken: function(form) {
			var _atlToken = form.find("input[name='atl_token']");
	        if (_atlToken.length > 0){
	            return _atlToken.val();
	        } else {
	            return atl_token();
	        }
		},
		
		attachRemoteFileError: function (_error, form) {
			var _attachRemoteFileField = form.find('#attach-remote-file-field');
			if (_error.length < 1) {
				_attachRemoteFileField.find(".error").remove();
			}
			else {
				_attachRemoteFileField.append(AJS.$('<div/>', {
					'class': 'error',
					text: _error
				}));
			}
		},
		
		initAddButtonsBinding: function() {
			AJS.$("button.attach-remote-file-add-image").on("click", function(e) {
				AttachRemoteFiles.addImageToCommentOrDescription($(this));
				return false;
			});
			AJS.$("button.attach-remote-file-add-thumbnail").on("click", function(e) {
				AttachRemoteFiles.addImageToCommentOrDescription($(this), true);
				return false;
			});
		},
		
		addImageToCommentOrDescription: function(object, thumbnail) {
			thumbnail = thumbnail || false;
			var _fileName = object.parent().parent().data("fileName");
			var _form = object.parents('form');
			
			var wikiTag = AttachRemoteFiles.getWikiImageTag(_fileName, thumbnail);
			var wikiField = AttachRemoteFiles.getWikiFieldObject(_form);
			
			if (wikiField) {
				wikiField.val(wikiField.val() + wikiTag);
			}
			
		},
		
		getWikiImageTag: function(fileName, thumbnail) {
			if (thumbnail == true) {
				return "!" + fileName + "|thumbnail!";
			}
			else {
				return "!" + fileName + "!";
			}
		},
		
		getWikiFieldObject: function(form) {
			var _commentField = form.find("#comment");
			var _descriptionField = form.find("#description");
			
			if (_commentField.length > 0) {
				return _commentField;
			}
			else if(_descriptionField.length > 0) {
				return _descriptionField;
			}
			else {
				return false;
			}
		}
};