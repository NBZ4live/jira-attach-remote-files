package com.mailrugames.jira.plugins.rest.util;

import com.atlassian.core.util.FileSize;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.XsrfCheckResult;
import com.atlassian.jira.security.xsrf.XsrfInvocationChecker;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import webwork.config.Configuration;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;

public class RemoteAttachmentHelper {
	
	private final XsrfInvocationChecker xsrfChecker;
    private final JiraAuthenticationContext authenticationContext;
    
	
	public RemoteAttachmentHelper(XsrfInvocationChecker xsrfChecker, JiraAuthenticationContext jiraAuthenticationContext)
	{
		this.xsrfChecker = xsrfChecker;
        this.authenticationContext = jiraAuthenticationContext;
	}
	
	public ValidationResult validate(final HttpServletRequest request, final String urlString) throws UnsupportedEncodingException
	{
		URL url;
		String fileName;
	    File tempFile;
	    Long size;
	    InputStream inputStream;
        String contentType;
		
		XsrfCheckResult xsrfCheckResult = xsrfChecker.checkWebRequestInvocation(request);
		
		if (xsrfCheckResult.isRequired() && !xsrfCheckResult.isValid())
        {
            return new ValidationResult(ValidationError.XSRF_TOKEN_INVALID, getText("xsrf.error.title"));

        }
		if (StringUtils.isBlank(urlString))
        {
            return new ValidationResult(ValidationError.URL_BLANK, null);
        }
		
		try {
			url = new URL(urlString);
		} catch (MalformedURLException e) {
			return new ValidationResult(ValidationError.URL_INVALID, e.getMessage());
		}
		
		fileName = getFileName(url);
		
		try {
			tempFile = File.createTempFile("remote-attachment-", "");
		} catch (IOException e) {
			return new ValidationResult(ValidationError.ATTACHMENT_IO_UNKNOWN, e.getMessage());
		}
		
		try {
			FileUtils.copyURLToFile(url, tempFile);
		} catch (IOException e) {
			return new ValidationResult(ValidationError.ATTACHMENT_IO_UNKNOWN, e.getMessage());
		}
		
		size = tempFile.length();
		
		if (size < 0)
		{
			return new ValidationResult(ValidationError.ATTACHMENT_IO_SIZE, getText("attachfile.error.io.size", fileName));
		}
		
		final Long largestAttachmentSize = new Long(getMaxAttachmentSize());
		if (size > largestAttachmentSize)
		{
			return new ValidationResult(ValidationError.ATTACHMENT_TO_LARGE, getText("attach.remote.file.size.toobig",
					fileName, FileSize.format(largestAttachmentSize)));
		}
		
		try {
			inputStream = new FileInputStream(tempFile);
		} catch (FileNotFoundException e) {
			return new ValidationResult(ValidationError.ATTACHMENT_IO_UNKNOWN, e.getMessage());
		}
		
		try {
			contentType = tempFile.toURI().toURL().openConnection().getContentType();
		} catch (MalformedURLException e) {
			return new ValidationResult(ValidationError.ATTACHMENT_IO_UNKNOWN, e.getMessage());
		} catch (IOException e) {
			return new ValidationResult(ValidationError.ATTACHMENT_IO_UNKNOWN, e.getMessage());
		}
		
		return new ValidationResult(inputStream, size, contentType, fileName);
	}
	
	String getMaxAttachmentSize()
    {
        return Configuration.getString(APKeys.JIRA_ATTACHMENT_SIZE);
    }
	
	private String getFileName(URL url) throws UnsupportedEncodingException
	{
		File fileObject = new File(url.getPath());
		String fileName = fileObject.getName();
		String decodedFileName = URLDecoder.decode(fileName, "UTF-8");
		return decodedFileName.replaceAll("[^a-zA-Z0-9.-]", "_");
	}
	
	private String getText(String text, Object... args)
    {
        return authenticationContext.getI18nHelper().getText(text, args);
    }
	
	public enum ValidationError
    {
        ATTACHMENT_TO_LARGE,
        ATTACHMENT_IO_SIZE,
        ATTACHMENT_IO_UNKNOWN,
        URL_BLANK,
        XSRF_TOKEN_INVALID,
        URL_INVALID
    }


    public static class ValidationResult
    {

        private final long size;
        private final InputStream inputStream;
        private final String contentType;
        private final String fileName;
        private final ValidationError errorType;
        private final String errorMessage;

        public ValidationResult(final InputStream inputStream, final long size, final String contentType, final String fileName)
        {
            this.inputStream = inputStream;
            this.size = size;
            this.contentType = contentType;
            this.fileName = fileName;
            this.errorType = null;
            this.errorMessage = null;
        }

        public ValidationResult(final ValidationError errorType, final String errorMessage)
        {
            this.inputStream = null;
            this.size = -1;
            this.contentType = null;
            this.fileName = "NaN";
            this.errorType = errorType;
            this.errorMessage = errorMessage;
        }

        public ValidationResult(final ValidationError errorType, final String errorMessage, final String fileName)
        {
            this.inputStream = null;
            this.size = -1;
            this.contentType = null;
            this.fileName = fileName;
            this.errorType = errorType;
            this.errorMessage = errorMessage;
        }

        public long getSize()
        {
            return size;
        }

        public InputStream getInputStream()
        {
            return inputStream;
        }

        public String getContentType()
        {
            return contentType;
        }
        
        public String getFileName()
        {
        	return fileName;
        }


        public ValidationError getErrorType()
        {
            return errorType;
        }

        public boolean isValid()
        {
            return errorType == null;
        }

        public String getErrorMessage()
        {
            return errorMessage;
        }
    }
}
