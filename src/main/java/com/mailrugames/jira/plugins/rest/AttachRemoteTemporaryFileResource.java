package com.mailrugames.jira.plugins.rest;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.bc.issue.IssueService;
import com.atlassian.jira.bc.project.ProjectService;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.attachment.TemporaryAttachment;
import com.atlassian.jira.project.Project;
import com.mailrugames.jira.plugins.rest.util.RemoteAttachmentHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.xsrf.XsrfInvocationChecker;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.web.util.AttachmentException;
import com.atlassian.jira.web.util.WebAttachmentManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import org.apache.commons.io.IOUtils;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Collection;

/**
 * A resource of message.
 */
@Path ("/")
@Produces (MediaType.APPLICATION_JSON)
@AnonymousAllowed
public class AttachRemoteTemporaryFileResource {
	
	private final JiraAuthenticationContext authContext;
    private final WebAttachmentManager webAttachmentManager;
    private final IssueService issueService;
    private final ProjectService projectService;
    private final XsrfInvocationChecker xsrfChecker;
    private final XsrfTokenGenerator xsrfGenerator;
    private final RemoteAttachmentHelper attachmentHelper;
	
	public AttachRemoteTemporaryFileResource(JiraAuthenticationContext authContext,
			WebAttachmentManager webAttachmentManager, IssueService issueService, ProjectService projectService,
            XsrfInvocationChecker xsrfChecker, XsrfTokenGenerator xsrfGenerator)
	{
		this.authContext = authContext;
        this.webAttachmentManager = webAttachmentManager;
        this.issueService = issueService;
        this.projectService = projectService;
        this.xsrfChecker = xsrfChecker;
        this.xsrfGenerator = xsrfGenerator;
        this.attachmentHelper = new RemoteAttachmentHelper(this.xsrfChecker, this.authContext);
	}
    
    @POST
    @Consumes (MediaType.WILDCARD)
    public Response addRemoteTemporaryAttachment(@QueryParam ("url") String urlString,
            @QueryParam ("projectId") Long projectId, @QueryParam ("issueId") Long issueId,
            @Context HttpServletRequest request) throws UnsupportedEncodingException
    {

    	RemoteAttachmentHelper.ValidationResult validationResult = attachmentHelper.validate(request, urlString);
    	
    	if (!validationResult.isValid())
    	{
    		if (validationResult.getErrorType() == RemoteAttachmentHelper.ValidationError.URL_BLANK)
    		{
    			return Response.status(Response.Status.BAD_REQUEST).build();
    		}
    		
    		if (validationResult.getErrorType() == RemoteAttachmentHelper.ValidationError.XSRF_TOKEN_INVALID)
    		{
    			return createTokenError(xsrfGenerator.generateToken(request));
    		}
    		else if  (validationResult.getErrorType() == RemoteAttachmentHelper.ValidationError.ATTACHMENT_IO_SIZE)
    		{
    			String message = authContext.getI18nHelper().getText("attachfile.error.io.size");
                return createError(Response.Status.BAD_REQUEST, message);
    		}
            else if (validationResult.getErrorType() == RemoteAttachmentHelper.ValidationError.ATTACHMENT_IO_UNKNOWN)
            {
                String message = authContext.getI18nHelper().getText("attachfile.error.io.error", validationResult.getFileName(), validationResult.getErrorMessage());
                return createError(Response.Status.INTERNAL_SERVER_ERROR, message);
            }
            else {
            	return createError(Response.Status.INTERNAL_SERVER_ERROR, validationResult.getErrorMessage());
            }
    	}
    	
    	if (issueId == null && projectId == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    	
    	Project project = null;
        Issue issue = null;
        
        final User user = authContext.getLoggedInUser();
        if (issueId != null)
        {
            issue = getIssue(user, issueId);
        }
        else
        {
            project = getProject(user, projectId);
        }
        
        InputStream inputStream = validationResult.getInputStream();
        
        try {
        	final TemporaryAttachment attach = webAttachmentManager.createTemporaryAttachment(validationResult.getInputStream(), validationResult.getFileName(),
                    validationResult.getContentType(), validationResult.getSize(), issue, project);
            return Response.status(Response.Status.CREATED)
                    .entity(new GoodResult(attach.getId(), validationResult.getFileName(), attach.getContentType())).build();
        }
        catch (AttachmentException e)
        {
            return createError(Response.Status.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        finally
        {
            IOUtils.closeQuietly(inputStream);
        }	
    }
    
    private Issue getIssue(User user, Long id)
    {
        IssueService.IssueResult result = issueService.getIssue(user, id);
        if (result.isValid())
        {
            return result.getIssue();
        }
        else
        {
            return throwFourOhFour(result.getErrorCollection());
        }
    }
    
    private Project getProject(User user, Long id)
    {
        ProjectService.GetProjectResult projectResult = projectService.getProjectById(user, id);
        if (projectResult.isValid())
        {
            return projectResult.getProject();
        }
        else
        {
            return throwFourOhFour(projectResult.getErrorCollection());
        }
    }
    
    private static Response createError(Response.Status status, com.atlassian.jira.util.ErrorCollection collection)
    {
        String message = getFirstElement(collection.getErrorMessages());
        if (message == null)
        {
            message = getFirstElement(collection.getErrors().values());
        }
        return createError(status, message);
    }
    
    private static Response createError(Response.Status status, String message)
    {
        return Response.status(status).entity(new BadResult(message)).build();
    }

    private Response createTokenError(String newToken)
    {
        String message = authContext.getI18nHelper().getText("attachfile.xsrf.try.again");
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .entity(new BadResult(message, newToken)).build();
    }
    
    private <T> T throwFourOhFour(com.atlassian.jira.util.ErrorCollection errorCollection)
    {
        throw new WebApplicationException(createError(Response.Status.NOT_FOUND, errorCollection));
    }
    
    private static <T> T getFirstElement(Collection<? extends T> values)
    {
        if (!values.isEmpty())
        {
            return values.iterator().next();
        }
        else
        {
            return null;
        }
    }
    
    @SuppressWarnings ( { "UnusedDeclaration" })
    @XmlRootElement
    public static class GoodResult
    {
        @XmlElement
        private String name;

        @XmlElement
        private String id;

        @XmlElement
        private String contentType;

        @SuppressWarnings ( { "UnusedDeclaration", "unused" })
        private GoodResult() {}

        GoodResult(long id, String name, String contentType)
        {
            this.id = String.valueOf(id);
            this.name = name;
            this.contentType = contentType;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }

            GoodResult that = (GoodResult) o;

            if (id != null ? !id.equals(that.id) : that.id != null) { return false; }
            if (name != null ? !name.equals(that.name) : that.name != null) { return false; }
            if (contentType != null ? !contentType.equals(that.contentType) : that.contentType != null) { return false; }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (id != null ? id.hashCode() : 0);
            return result;
        }
    }

    @SuppressWarnings ( { "UnusedDeclaration" })
    @XmlRootElement
    public static class BadResult
    {
        @XmlElement
        private String errorMessage;

        @XmlElement
        private String token;

        @SuppressWarnings ( { "UnusedDeclaration", "unused" })
        private BadResult() {}

        BadResult(String msg)
        {
            this(msg, null);
        }

        BadResult(String msg, String token)
        {
            this.errorMessage = msg;
            this.token = token;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) { return true; }
            if (o == null || getClass() != o.getClass()) { return false; }

            BadResult badResult = (BadResult) o;

            if (errorMessage != null ? !errorMessage.equals(badResult.errorMessage) : badResult.errorMessage != null)
            { return false; }
            if (token != null ? !token.equals(badResult.token) : badResult.token != null) { return false; }

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = errorMessage != null ? errorMessage.hashCode() : 0;
            result = 31 * result + (token != null ? token.hashCode() : 0);
            return result;
        }
    }
}